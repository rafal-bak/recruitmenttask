﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;

namespace RecruitmentTask.Web
{
    public static class ContainerInitializer
    {
        public static void Initialize()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly);

            RegisterTypes(builder);

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            //example
            //builder.RegisterType<HelloService>().AsImplementedInterfaces().InstancePerRequest();
        }
    }
}